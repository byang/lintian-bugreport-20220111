-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: fcitx5-table-extra
Binary: fcitx5-table, fcitx5-table-array30, fcitx5-table-array30-large, fcitx5-table-boshiamy, fcitx5-table-cangjie-large, fcitx5-table-cangjie3, fcitx5-table-cangjie5, fcitx5-table-cantonese, fcitx5-table-cantonhk, fcitx5-table-easy-large, fcitx5-table-jyutping, fcitx5-table-quick-classic, fcitx5-table-quick3, fcitx5-table-quick5, fcitx5-table-scj6, fcitx5-table-stroke5, fcitx5-table-t9, fcitx5-table-wu, fcitx5-table-wubi-large, fcitx5-table-zhengma, fcitx5-table-zhengma-large, fcitx5-table-zhengma-pinyin
Architecture: all
Version: 5.0.6-1
Maintainer: Lu YaNing <dluyaning@gmail.com>
Homepage: https://github.com/fcitx/fcitx5-table-extra
Standards-Version: 4.6.0.1
Vcs-Browser: https://salsa.debian.org/Dami/fcitx5-table-extra
Vcs-Git: https://salsa.debian.org/Dami/fcitx5-table-extra.git
Build-Depends: cmake, debhelper-compat (= 13), extra-cmake-modules, libboost-dev, libfcitx5core-dev, libfcitx5utils-dev, libimecore-dev, libimetable-dev, libime-bin
Package-List:
 fcitx5-table deb utils optional arch=all
 fcitx5-table-array30 deb utils optional arch=all
 fcitx5-table-array30-large deb utils optional arch=all
 fcitx5-table-boshiamy deb utils optional arch=all
 fcitx5-table-cangjie-large deb utils optional arch=all
 fcitx5-table-cangjie3 deb utils optional arch=all
 fcitx5-table-cangjie5 deb utils optional arch=all
 fcitx5-table-cantonese deb utils optional arch=all
 fcitx5-table-cantonhk deb utils optional arch=all
 fcitx5-table-easy-large deb utils optional arch=all
 fcitx5-table-jyutping deb utils optional arch=all
 fcitx5-table-quick-classic deb utils optional arch=all
 fcitx5-table-quick3 deb utils optional arch=all
 fcitx5-table-quick5 deb utils optional arch=all
 fcitx5-table-scj6 deb utils optional arch=all
 fcitx5-table-stroke5 deb utils optional arch=all
 fcitx5-table-t9 deb utils optional arch=all
 fcitx5-table-wu deb utils optional arch=all
 fcitx5-table-wubi-large deb utils optional arch=all
 fcitx5-table-zhengma deb utils optional arch=all
 fcitx5-table-zhengma-large deb utils optional arch=all
 fcitx5-table-zhengma-pinyin deb utils optional arch=all
Checksums-Sha1:
 bff7da3bb9322529acade77059849b1e4bc75a30 8489591 fcitx5-table-extra_5.0.6.orig.tar.gz
 d8a52782b9b99f49b5b2687c69c7ef9af4f64d8a 3120 fcitx5-table-extra_5.0.6-1.debian.tar.xz
Checksums-Sha256:
 d962b6a0c448ed0a235c2b7ca4c17534b0e8b8c5f0c500cd3a45492596615761 8489591 fcitx5-table-extra_5.0.6.orig.tar.gz
 f26e1cdf302f35b06c86076d5e2c7de41c07cbbab14b7e6eeb43bd4ae69ba121 3120 fcitx5-table-extra_5.0.6-1.debian.tar.xz
Files:
 fd7db60ae678b94607dae78a6a3e2752 8489591 fcitx5-table-extra_5.0.6.orig.tar.gz
 d279730f7020a5509231d6e5571108aa 3120 fcitx5-table-extra_5.0.6-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEHool/OtTelIpSYIFsxW+0vwyk0QFAmGsy6gACgkQsxW+0vwy
k0QGyA/+I9q9j/iNmHkaoq1zTMPq27AmnXryvL3QuuJLmk4w9kD4QGxVH0CZr+CE
IkwZmFjuh4G2GDQfFDEVP/+rZV87OKSW9brdHLLhP3u7o8WCKMOr1mk7C2VtVXvY
j8MR9AbsmfwXQ1FLNwp9dRy7KE93kMrxxM+4rBb8KaDOgx+I/4Sye4/b2eNF4BwA
V39Qocp3Qnyth9gN6/ux9VJ94HLKKkSi2xh3D5CLu5GjsF+kKEAHDfCCMD10RhG4
lH/0xlSUaQ5PFnt+LawQGpAqJ9jUcNqpUJ2Tx0zbj6tEE5VBF19DZLP6LWnRljrC
la1uoA/fox71948EGtS5UdWDC1vJuwCsSiq2/xse4hBmztAPCyyFwGSoWqoZwSFA
LEowzezhgbbbwtgQFAAhrl8ZQPwY3RSkY10GWVIv84rg1XQC8L3BAkGrXvCaPN1G
Ex39kZlt4bFNM0ZCMHmTYj7meQY2xcVhOwOm7KlOLAPZjcoc9tvngvaQld/lKqr4
Wu0C51uLBWwxK2q6+TwW9RrGNYs8smiC6g5F5Gho7DvwJ1lIDpa6JpXz5h6lvjZg
4qXe4E54oZsOQu84I71kQfjj4nG0yqk+xVBDK2EYlnPj06I0cEcjULHnWSfIMqSa
GPUwt0GOrgmGIoxegQOAVHEwVqowC5TdtuWRrPJmo5jM+IGYLpw=
=3CSM
-----END PGP SIGNATURE-----
